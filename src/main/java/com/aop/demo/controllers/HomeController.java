package com.aop.demo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HomeController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/hello")
    public String getHello(@RequestHeader HttpHeaders headers) {
        MediaType contentType = headers.getContentType();

        logger.info("request date " + contentType);

        return "Hello AOP World";
    }
}
